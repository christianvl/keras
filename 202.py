### Regression models with keras

# Download and clean dataset
import pandas as pd
import numpy as np

# Download data and read it as a Pandas dataframe
concrete_data = pd.read_csv("https://s3-api.us-geo.objectstorage.softlayer.net/cf-courses-data/CognitiveClass/DL0101EN/labs/data/concrete_data.csv")
concrete_data.head()

# check how many data points there are
concrete_data.shape

# So, there are approximately 1000 samples to train our model on. Because of
# the few samples, we have to be careful not to over fit the training data.
# Let's check the dataset for any missing values
concrete_data.describe()
concrete_data.isnull().sum()

# split data into predictors and target
# target is the concrete sample strength. Predictors will be all other columns.
concrete_data_columns = concrete_data.columns

predictors = concrete_data[concrete_data_columns[concrete_data_columns != 'Strength']]
target = concrete_data['Strength']

predictors.head()
target.head()

# normalize the data by substracting the mean and dividing by the standard
# deviation.
predictors_norm = (predictors - predictors.mean()) / predictors.std()
predictors_norm.head()

# save the number of predictors to n_cols_ since we will need this number when
# building our network.
n_cols = predictors_norm.shape[1]

# import keras
import keras  # upgrade to TensorFlow, defaults to  Theano

from keras.models import Sequential
from keras.layers import Dense

# Build Neural Network
# function to create regression model
def regression_model():
    # create model
    model = Sequential()
    model.add(Dense(50, activation = "relu", input_shape = (n_cols,)))
    model.add(Dense(50, activation = "relu"))
    model.add(Dense(1))
    # compile model
    model.compile(optimizer = "adam", loss = "mean_squared_error")
    return model

# Train and test network
model = regression_model()

# train and test the model at the same time using the _fit_ method. We will
# leave out 30% of the data for validation and we will train the model for 100
# epochs.
# fit the model
model.fit(predictors_norm, target, validation_split = 0.3, epochs = 100, verbose = 2)
