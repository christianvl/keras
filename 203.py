### Classification models

#  1. Use of MNIST database for training various image processing systems
#  2. Build a Neural Network
#  3. Train and Test the Network

from tensorflow import keras

from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
from tensorflow.keras.utils import to_categorical

import matplotlib.pyplot as plt

# import dataset from keras API
from tensorflow.keras.datasets import mnist

(X_train, y_train), (X_test, y_test) = mnist.load_data()

X_train.shape

# The first number in the output tuple is the number of images, and the other
# two numbers are the size of the images in datset. So, each image is 28 pixels
# by 28 pixels.

# Let's visualize the first image in the training set using Matplotlib's
# scripting layer.
plt.imshow(X_train[0])

# With conventional neural networks, we cannot feed in the image as input as
# is. So we need to flatten the images into one-dimensional vectors, each of
# size 1 x (28 x 28) = 1 x 784.
num_pixels = X_train.shape[1] * X_train.shape[2]  # find the size of one-dimensional vector

X_train = X_train.reshape(X_train.shape[0], num_pixels).astype('float32')
X_test = X_test.reshape(X_test.shape[0], num_pixels).astype('float32')

# Since pixel values can range from 0 to 255, let's normalize the vectors to be
# between 0 and 1.
X_train = X_train / 255
X_test = X_test / 255

# Finally, before we start building our model, remember that for classification
# we need to divide our target variable into categories. We use the
# to_categorical function from the Keras Utilities package.
y_train = to_categorical(y_train)
y_test = to_categorical(y_test)

num_classes = y_test.shape[1]
print(num_classes)

### Build Neural Network
def classification_model():
    model = Sequential()
    model.add(Dense(num_pixels, activation = "relu", input_shape = (num_pixels,)))
    model.add(Dense(100, activation = "relu"))
    model.add(Dense(num_classes, activation = "softmax"))
    model.compile(optimizer = "adam", loss = "categorical_crossentropy", metrics = ["accuracy"])
    return model

### Train and test
# build model
model = classification_model()

# fit the model
model.fit(X_train, y_train, validation_data=(X_test, y_test), epochs=10, verbose=2)

# evaluate model
scores = model.evaluate(X_test, y_test, verbose = 0)

# print accuracy and corresponding error
print("Accuracy: {} \n Error: {}".format(scores[1], 1 - scores[1]))

# save the model after training to save computation resources
model.save("classification_model.h5")
# to use a saved model
# from tensorflow.keras.models import load_model
# pretrained_model = load_model("classification_model.h5")
