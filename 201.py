### Regression models with Keras - NOT FOR RUNNING

import keras
from keras.models import Sequential  # for linear stack of layers
from keras.layers import Dense # layer type

# call constructor to create model
model = Sequential()

n_cols = concrete_data.shape[1]

# add layers
# use ReLU for hidden layers
# for the first layer, pass input_shape = number of columns or predictors
model.add(Dense(5, activation="relu", input_shape=(n_cols,)))
# second hidden layer
model.add(Dense(5, activation="relu"))
# output layer
model.add(Dense(1))

# for training, define optimizer and error metric
model.compile(optimizer = "adam", loss = "mean_squared_error")
model.fit(predictors, target)

predictions = model.predict(test_data)
