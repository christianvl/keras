#Week 4 lab - convolutional neural networks
### if > E tensorflow/stream_executor/cuda/cuda_dnn.cc:328] Could not create cudnn handle: CUDNN_STATUS_INTERNAL_ERROR
### $> export TF_FORCE_GPU_ALLOW_GROWTH='true'

### import packages

# import keras
# from keras.models import Sequential
# from keras.layers import Dense
# from keras.utils import to_categorical

from tensorflow import keras
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
from tensorflow.keras.utils import to_categorical

# convolutional requires extra packages
from tensorflow.keras.layers import Conv2D
from tensorflow.keras.layers import MaxPooling2D
from tensorflow.keras.layers import Flatten

### Convolutional Layer with One set of convolutional and pooling layers
# import data
from tensorflow.keras.datasets import mnist

# load data
(X_train, y_train), (X_test, y_test) = mnist.load_data()

# reshape to be [samples][pixels][width][height]
X_train = X_train.reshape(X_train.shape[0], 28, 28, 1).astype("float32")
X_test = X_test.reshape(X_test.shape[0], 28, 28, 1).astype("float32")

# normalize pixel values to 0 / 1
X_train = X_train / 255
X_test = X_test / 255

# convert target variable to binary
y_train = to_categorical(y_train)
y_test = to_categorical(y_test)
num_classes = y_test.shape[1]

# Function to create model (convolutional layer with one set of convolutional and pooling)
def convolutional_model():
    # create
    model = Sequential()

    model.add(Conv2D(16, (5, 5), strides=(1, 1), activation = "relu", input_shape = (28, 28, 1)))
    model.add(MaxPooling2D(pool_size = (2, 2), strides=(2, 2)))

    model.add(Flatten())
    model.add(Dense(100, activation = "relu"))
    model.add(Dense(num_classes, activation = "softmax"))
    #compile
    model.compile(optimizer="adam", loss="categorical_crossentropy", metrics=["accuracy"])
    return model

# # call function to create model
# model = convolutional_model()

# # fit
# model.fit(X_train, y_train, validation_data=(X_test, y_test), epochs=10, batch_size=200, verbose=2)

# # evaluate
# scores = model.evaluate(X_test, y_test, verbose=0)
# print("Accuracy: {} \n Error: {}".format(scores[1], 100 - scores[1] * 100))

# Function to create model (convolutional layer with two sets of convolutional and pooling)
def convolutional_model2():
    # create
    model = Sequential()

    model.add(Conv2D(16, (5, 5), strides=(1, 1), activation = "relu", input_shape = (28, 28, 1)))
    model.add(MaxPooling2D(pool_size = (2, 2), strides=(2, 2)))

    model.add(Conv2D(8, (5, 5), activation = "relu"))
    model.add(MaxPooling2D(pool_size = (2, 2), strides=(2, 2)))

    model.add(Flatten())
    model.add(Dense(100, activation = "relu"))
    model.add(Dense(num_classes, activation = "softmax"))
    #compile
    model.compile(optimizer="adam", loss="categorical_crossentropy", metrics=["accuracy"])
    return model

# call function to create model
model2 = convolutional_model2()

# fit
model2.fit(X_train, y_train, validation_data=(X_test, y_test), epochs=10, batch_size=200, verbose=2)

# evaluate
scores = model2.evaluate(X_test, y_test, verbose=0)
print("Accuracy: {} \n Error: {}".format(scores[1], 100 - scores[1] * 100))

# 1 set
# 300/300 - 1s - loss: 0.2762 - accuracy: 0.9200 - val_loss: 0.1071 - val_accuracy: 0.9669
# Epoch 2/10
# 300/300 - 1s - loss: 0.0847 - accuracy: 0.9756 - val_loss: 0.0618 - val_accuracy: 0.9811
# Epoch 3/10
# 300/300 - 1s - loss: 0.0582 - accuracy: 0.9831 - val_loss: 0.0559 - val_accuracy: 0.9823
# Epoch 4/10
# 300/300 - 1s - loss: 0.0458 - accuracy: 0.9863 - val_loss: 0.0439 - val_accuracy: 0.9855
# Epoch 5/10
# 300/300 - 1s - loss: 0.0379 - accuracy: 0.9886 - val_loss: 0.0415 - val_accuracy: 0.9862
# Epoch 6/10
# 300/300 - 1s - loss: 0.0313 - accuracy: 0.9906 - val_loss: 0.0410 - val_accuracy: 0.9865
# Epoch 7/10
# 300/300 - 1s - loss: 0.0282 - accuracy: 0.9915 - val_loss: 0.0431 - val_accuracy: 0.9863
# Epoch 8/10
# 300/300 - 1s - loss: 0.0229 - accuracy: 0.9932 - val_loss: 0.0420 - val_accuracy: 0.9868
# Epoch 9/10
# 300/300 - 1s - loss: 0.0194 - accuracy: 0.9942 - val_loss: 0.0411 - val_accuracy: 0.9865
# Epoch 10/10
# 300/300 - 1s - loss: 0.0166 - accuracy: 0.9951 - val_loss: 0.0397 - val_accuracy: 0.9870
# Accuracy: 0.9869999885559082 
#  Error: 1.3000011444091797

# 2 sets
# 300/300 - 1s - loss: 0.4383 - accuracy: 0.8774 - val_loss: 0.1406 - val_accuracy: 0.9574
# Epoch 2/10
# 300/300 - 1s - loss: 0.1187 - accuracy: 0.9639 - val_loss: 0.0838 - val_accuracy: 0.9745
# Epoch 3/10
# 300/300 - 1s - loss: 0.0842 - accuracy: 0.9750 - val_loss: 0.0750 - val_accuracy: 0.9753
# Epoch 4/10
# 300/300 - 1s - loss: 0.0692 - accuracy: 0.9789 - val_loss: 0.0511 - val_accuracy: 0.9844
# Epoch 5/10
# 300/300 - 1s - loss: 0.0571 - accuracy: 0.9828 - val_loss: 0.0461 - val_accuracy: 0.9846
# Epoch 6/10
# 300/300 - 1s - loss: 0.0504 - accuracy: 0.9845 - val_loss: 0.0454 - val_accuracy: 0.9850
# Epoch 7/10
# 300/300 - 1s - loss: 0.0447 - accuracy: 0.9862 - val_loss: 0.0385 - val_accuracy: 0.9867
# Epoch 8/10
# 300/300 - 1s - loss: 0.0396 - accuracy: 0.9876 - val_loss: 0.0402 - val_accuracy: 0.9873
# Epoch 9/10
# 300/300 - 1s - loss: 0.0350 - accuracy: 0.9893 - val_loss: 0.0329 - val_accuracy: 0.9896
# Epoch 10/10
# 300/300 - 1s - loss: 0.0322 - accuracy: 0.9898 - val_loss: 0.0469 - val_accuracy: 0.9855
# Accuracy: 0.9854999780654907 
#  Error: 1.4500021934509277
